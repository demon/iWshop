<?php

include_once dirname(__FILE__) . '/../system/Model.php';
include_once dirname(__FILE__) . '/Db.php';

/*
 * Copyright (C) 2014 <koodo@qq.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

/**
 * Description of mWdmin
 *
 * @author <koodo@qq.com>
 */
class mWdmin extends Model {

    /**
     * login qrcodeScan listen
     * @param type $code
     */
    public function loginQrcodeScaned($code, $openid) {
        if (!isset($this->Db)) {
            $this->Db = new Db();
        }
        $code = substr($code, 4);
        $this->Db->query(sprintf("UPDATE `admin_login_code_token` SET used = 1,bind = '%s' WHERE tid = %s;", $openid, $code));
        $access_token = WechatSdk::getServiceAccessToken();
        Messager::sendText($access_token, $openid, '后台扫描登录成功！');
    }

    public function genLoginToken($ip) {
        
    }

}
