<?php

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

class User extends Model {

    const MANT_BALANCE_ADD = '+';
    const MANT_BALANCE_DIS = '-';

    /**
     * 
     * @param type $userId
     */
    public function deleteUser($userId) {
        
    }

    /**
     * 
     * @param type $userId
     * @param type $modifyData
     */
    public function modifyUser($userId, $modifyData = array()) {
        
    }

    /**
     * 
     * @param type $userData
     */
    public function createUser($userData = array()) {
        
    }

    /**
     * 
     * @param type $uid
     * @return <object>
     */
    public function getUserInfo($uid = false) {
        if (!$uid) {
            $uid = $this->pCookie("uid");
        }
        $userInfosq = $this->Db->query("SELECT cs.client_address,cl.client_id,cl.headimg,cs.client_wechat_openid,cs.client_name,cs.client_money from client_infos cl
        LEFT JOIN clients cs ON cs.client_id =  cl.client_id WHERE cl.client_id = " . $uid);
        $info = new stdClass();
        $info->uid = (int) $userInfosq[0]['client_id'];
        $info->uhead = $userInfosq[0]['headimg'];
        $info->nickname = $userInfosq[0]['client_name'];
        $info->address = $userInfosq[0]['client_address'];
        $info->balance = (float) $userInfosq[0]['client_money'];
        unset($userInfosq);
        return $info;
    }

    /**
     * 用户余额操作
     * @param type $amount
     * @param type $uid
     * @param type $type
     */
    public function mantUserBalance($amount, $uid, $type = self::MANT_BALANCE_ADD) {
        $mSql = sprintf("UPDATE `clients` SET `client_money` = `client_money` $type $amount WHERE `client_id` = $uid;");
        if ($this->Db->query($mSql) !== false) {
            if($type === self::MANT_BALANCE_DIS)
                $amount = -$amount;
            $rSql = "INSERT INTO `client_money_record` (`client_id`,`amount`,`time`) VALUES ($uid ,$amount ,NOW());";
            return $this->Db->query($rSql);
        }
    }

}
