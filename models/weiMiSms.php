<?php

/*
 * Copyright (C) 2014 Administrator.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

class weiMiSms extends Model {

    /**
     * @param type $phone
     * @param type $cid 模板id
     * @param type $pm  模板参数
     * @return <array>
     */
    public function sendSmsTemplate($phone, $cid, $pm) {
        // pack params
        $phone = str_replace(' ', '', $phone);
        $paramp = "";
        if (is_array($pm)) {
            foreach ($pm as $index => $p) {
                $paramp .= ("&p" . ($index + 1) . "=$p");
            }
        } else {
            $paramp = '&p1=' . $pm;
        }
        // pack params end
        $curl = curl_init();
        $url = "http://www.weimi.cc/iface/sms.html?uid=" . WEIMIUID . "&pas=" . WEIMIKEY . "&type=json&mob=$phone&cid=$cid$paramp";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode($res, true);
    }

    /**
     * 发送短信，非模板
     * @param type $phone
     * @param type $cont
     * @return <array>
     */
    public function sendSms($phone, $cont) {
        $phone = str_replace(' ', '', $phone);
        $curl = curl_init();
        $url = "http://www.weimi.cc/iface/sms.html?uid=" . WEIMIUID . "&pas=" . WEIMIKEY . "&type=json&mob=$phone&con=$cont";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode($res, true);
    }

    /**
     * 获取短信使用情况
     * @return <array>
     */
    public function getSmsUseAge() {
        $curl = curl_init();
        $url = "http://www.weimi.cc/iface/account.html?uid=" . WEIMIUID . "&pas=" . WEIMIKEY . "&type=json";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode(str_replace('""', '","', $res), true);
    }

}
