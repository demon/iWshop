<?php

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

/**
 * 商品主类
 */
class Product extends Model {

    /**
     * 
     * @param type $productId
     */
    public function modifyProduct($productId) {
        
    }

    /**
     * 
     * @param type $productId
     */
    public function deleteProduct($productId) {
        
    }

    /**
     * 
     * @param type $productData
     */
    public function createProduct($productData = array()) {
        
    }

    /**
     * 
     * @param type $productId
     */
    public function getById($productId) {
        
    }

    /**
     * 
     * @param <string> $limitStr
     * @return <array> list
     */
    public function getProductList($orderby, $limitStr) {
        !isset($orderby) && $orderby = '`product_start` DESC';
        $SQL = sprintf("SELECT * FROM `vProductInfo` WHERE `product_id` <> 7878 ORDER BY %s LIMIT %s;", $orderby, $limitStr);
        return $this->Db->query($SQL);
    }

    /**
     * 获取商品分类信息
     * @param type $catid
     * @return <string>
     */
    public function getCatInfo($catid) {
        $catid = addslashes(intval($catid));
        $SQL = "SELECT * FROM `product_category` WHERE `cat_id` = $catid; ORDER BY `cat_order` DESC";
        $res = $this->Db->query($SQL);
        return $res[0];
    }

    /**
     * 获取商品分类列表
     * @param type $limit
     * @return <array>
     */
    public function getCatList() {
        $SQL = "SELECT * FROM `product_category`;";
        return $this->Db->query($SQL);
    }

}
