<?php

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

/**
 * 类加载器
 */
class ClassLoader {

    /**
     * 加载目标类文件
     * @global type $config
     * @accerss public static
     */
    public static function loadModuleFile($className, $Controller = false) {
        global $config;
        $_path = null;
        $_loaded = false;
        foreach ($config->classRoot as $_classRoot) {
            $_path = $_classRoot . $className . ".php";
            if (is_file($_path)) {
                include_once $_path;
                if($Controller){
                    $Controller->$className = new $className();
                }
                $_loaded = true;
                break;
            }
        }
        if (!$_loaded && $config->debug) {
            // echo 'Class file' . $_path . 'not found!<br>';
            self::loadModuleFile(strtolower($className));
            return false;
        } else {
            return true;
        }
        unset($_path);
        unset($_loaded);
    }
}

class runtime {

    var $StartTime = 0;
    var $StopTime = 0;

    function get_microtime() {
        list($usec, $sec) = explode(' ', microtime());
        return ((float) $usec + (float) $sec);
    }

    function start() {
        $this->StartTime = $this->get_microtime();
    }

    function stop() {
        $this->StopTime = $this->get_microtime();
    }

    function spent() {
        return round(($this->StopTime - $this->StartTime) * 1000, 1);
    }

}
