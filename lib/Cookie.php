<?php

/* 
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */



/**
 * Cookie 访问模块
 */
class Cookie{
    
    /**
     * @todo filter
     * @param type $name
     * @param type $value
     */
    public function set($name,$value){
        $_COOKIE[$name] = $value;
    }
    
    /**
     * 
     * @param type $name
     * @return type
     */
    public function get($name){
        return $_COOKIE[$name];
    }
    
}