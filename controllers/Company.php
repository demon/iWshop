<?php

/*
 * Copyright (C) 2014 Administrator.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

class Company extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('Db');
    }

    // 添加一个com推广记录
    public function addComSpread() {
        $productId = $_POST['productId'];
        $comId = $_POST['comId'];
        $Uin = "SELECT COUNT(`rid`) AS `count` FROM " . COMPANY_SPREAD . "WHERE `product_id` = $productId AND `com_id` = '$comId';";
        $Uin = $this->Db->query($Uin);
        // 生成记录
        if($Uin[0]['count'] == 0){
            $SQL = "INSERT INTO ".COMPANY_SPREAD." (`product_id`,`com_id`) VALUES ($productId,'$comId');";
            echo $this->Db->query($SQL);
        } else {
            // 已经有记录了
            echo 0;
        }
    }

}
