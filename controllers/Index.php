<?php

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

class Index extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('Db');
    }

    public function index($Query) {
        $this->getOpenId();
        // 推荐com，90分钟
        if (!isset($_COOKIE['com']) && isset($Query->com)) {
            setcookie("com", $Query->com, time() + 5400);
        }

        $this->loadModel('Product');
        $this->Product->hook(array($this->Db));

        // product categorys
        $catList = $this->Product->getCatList();

        // hotProduct
        $productHot = $this->Product->getProductList('`sale_count` DESC', 6);

        // newProduct
        $productNew = $this->Product->getProductList('`product_start` DESC', 6);
        $this->Smarty->assign('productHot', $productHot);
        $this->Smarty->assign('productNew', $productNew);
        $this->Smarty->assign('catList', $catList);
        $this->show();
    }

}
