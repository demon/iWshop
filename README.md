marked(html, {breaks: true});

#Wshop
微信开源商城，采用自主开发的轻量级MVC框架，多年全端经验倾力打造。
<p>
首页截图:
<p>
![home-pic](./static/images/projectDesc/home-p1.png)
<p>
目前开发中的客户：
<p>
![home-pic](./static/images/projectDesc/qrcode_for_gh_09a0de3f4af6_430.jpg)