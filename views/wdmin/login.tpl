<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>微点客户登录</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
        <meta name="format-detection" content="telephone=no"/>
        <link href="https://res.wx.qq.com/mpres/htmledition/images/favicon1e5b3a.ico" rel="Shortcut Icon" />
        <link href="static/css/wadmin.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/wdmin.js?v={$cssversion}"></script>
    </head>
    <body class="wdmin-login">
        <input type="hidden" id="qrcode-co" value="{$ip}" />
        <div id="qrcode-login-wrapp">
            <img src="{$qrcode}" width="301px" height="301px"/>
            <div id="qrcode-login-desc">微信扫描二维码以登录</div>
        </div>
    </body>
</html>